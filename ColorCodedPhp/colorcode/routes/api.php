<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Users;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register',[Users::class,'SignUp']);
Route::post('login',[Users::class,'Login']);
Route::post('gender',[Users::class,'updateGender']);
Route::get('quiz',[Users::class,'getQuizData']);
Route::post('forgetpassword', [Users::class, 'ForgetPassword']);
Route::post('verifyotp', [Users::class, 'VerifyOTP']);
Route::post('updatepassword', [Users::class, 'ChangePassword']);
Route::post('delete', [Users::class, 'DeleteAccount']);
