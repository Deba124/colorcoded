<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Quiz;
use Illuminate\Support\Facades\Mail;

class Users extends Controller
{
   public static $email = "";
   public static $otp = "";
    public function SignUp(Request $request)
    {
        $usr = User::where('email',$request->email)->first();

        if($usr)
        {

            if($usr->gender == null)
            {
                 return response([
                "response_code"=>400,
                "response_msg"=>"Email already exists!",
                "response_data"=>$usr
                ]);
            }
            return response([
                "response_code"=>400,
                "response_msg"=>"Email already exists!",
                "response_data"=>null
                ]);
        }
        else
        {
            $u= User::insert(["email"=>$request->email,"password"=>$request->pass]);
            if($u){
                $dat = User::where('email',$request->email)->first();
                return response([
                "response_code"=>200,
                "response_msg"=>"User created!",
                "response_data"=>$dat
                ]);

            }
            else
            {
                 return response([
                "response_code"=>400,
                "response_msg"=>"Can't create user!",
                "response_data"=>null
                ]);
            }
            

        }
    }

    public function updateGender(Request $request)
    {
        $usr = User::where('id',$request->id)->first();
        if(!$usr)
        {
            return response([
                "response_code"=>400,
                "response_msg"=>"User does'nt exists!",
                "response_data"=>null
                ]);
        }
        else
        {
            User::where('id',$request->id)->update(["gender"=>$request->gender,"name"=>$request->name]);
            $dat = User::where('email',$request->id)->first();
            return response([
                "response_code"=>200,
                "response_msg"=>"User created!",
                "response_data"=>$dat
                ]);
        }
    }

    public function Login(Request $request)
    {
        $usr = User::where('email',$request->email)->first();

        if(!$usr)
        {
            return response([
                "response_code"=>400,
                "response_msg"=>"Invalid email address!",
                "response_data"=>null
                ]);
        }

        if($request->pass == $usr->password)
        {
            return response([
                "response_code"=>200,
                "response_msg"=>"Login successfull!",
                "response_data"=>$usr
                ]);
        }
        else
        {
             return response([
                "response_code"=>400,
                "response_msg"=>"Invalid password!",
                "response_data"=>null
                ]);
        }
    }

    public function getQuizData()
    {
        $dat = Quiz::select('*')->get();
      
        return response([
    "response_data"=>$dat
],200);

    }
    

    public function ForgetPassword(Request $request)
    {
        $usr = User::where('email',$request->email)->first();
        Users::$otp = rand(100000,999999);
        $data = array('name'=>Users::$otp);
        Users::$email = $request->email;
        if($usr)
        {
            Mail::send(['html'=>'Otp'],$data,function($message){
                $message->to(Users::$email,"Color coded otp verify")->subject("Your One Time Password to reset password");
                $message->from('colorcodedkitcheninfo@gmail.com','Color coded kitchen');
            });

            User::where('email', $request->email)->update(['otp' => Users::$otp]);

            return response([
                "response_code"=>"200",
                "response_msg"=>"otp send successfull",
                "response_data"=>Users::$otp
            ],200);
            
        }

        return response([
            "response_code"=>"201",
            "response_msg"=>"No account exists with this email",
            "response_data"=>null
        ],201);
    }

    public function VerifyOTP(Request $request)
    {
        $usr = User::where('email',$request->email)->first();
        if($usr)
        {
            if($usr->otp == $request->otp)
            {
                return response([
                    "response_code"=>"200",
                    "response_msg"=>"otp verified successfully",
                    "response_data"=>null
                ],200);
            }

            return response([
                "response_code"=>"201",
                "response_msg"=>"Invalid otp",
                "response_data"=>null
            ],201);
        }

        return response([
            "response_code"=>"201",
            "response_msg"=>"No account exists with this email",
            "response_data"=>null
        ],201);
    }

    public function ChangePassword(Request $request)
    {
        $usr = User::where('email',$request->email)->update(['password'=>$request->password]);

        if($usr)
        {
            return response([
                "response_code"=>"200",
                "response_msg"=>"password changed successfully",
                "response_data"=>null
            ],200);
        }

        return response([
            "response_code"=>"201",
            "response_msg"=>"password did'nt change",
            "response_data"=>null
        ],201);
    }

    public function DeleteAccount(Request $request)
    {
        $usr = User::where('id', $request->id)->delete();

        if($usr)
        {
            return response("deleted", 200);
        }
        
        return response("Something went wrong! Try again", 201);
    }

}
